﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Result : MonoBehaviour
{
    public GameObject matrixdisplay;
    public GameObject gamecontroller;
    public Text result;
    public GameObject newgamebutton;
    public int resultcount = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Matrix mt = matrixdisplay.GetComponent<Matrix>();
        Maisu mi = gamecontroller.GetComponent<Maisu>();
        resultcount = 0;
        for(int i = 0; i  < mt.goban.GetLength(0); i++){
            for(int j = 0; j < mt.goban.GetLength(0); j++){
                if(mt.goban[i,j] == 1 || mt.goban[i,j] == -1){
                    resultcount += 1;
                }
            }
        }

        if(resultcount == 64){
            if(mi.shiromaisu_kari > mi.kuromaisu_kari){
                result.text = mi.shiromaisu_kari.ToString() + "対" + mi.kuromaisu_kari.ToString() + "で白の勝ち!!!!!";
            }
            else if(mi.shiromaisu_kari < mi.kuromaisu_kari){
                result.text = mi.kuromaisu_kari.ToString() + "対" + mi.shiromaisu_kari.ToString() + "で黒の勝ち!!!!!";
            }
            else{
                result.text = "引き分け!!!!!";
            }
            newgamebutton.SetActive (true);
        }
        
    }
}
