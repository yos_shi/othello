﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ReturnHantei_VSComputer : MonoBehaviour
{
    public int roop = 0;
    public int random_x = 0;
    public int random_z = 0;
    public int[] aki_x = new int[100];
    public int[] aki_z = new int[100];
    public int aki_1 = 0;
    public int aki_2 = 0;
    public int aki_length = 0;
    public GameObject position;
    public GameObject matrixdisplay;
    public GameObject gamecontroller;
    public GameObject planecreate;
    public int right_flag = 0;
    public int right = 0;
    public int left_flag = 0;
    public int left = 0;
    public int up_flag = 0;
    public int up = 0;
    public int down_flag = 0;
    public int down = 0;
    public int up_right_flag = 0;
    public int up_right = 0;
    public int down_left_flag = 0;
    public int down_left = 0;
    public int up_left_flag = 0;
    public int up_left = 0;
    public int up_left_number = 1;
    public int down_right_flag = 0;
    public int down_right = 0;
    public int down_right_number = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PutIshi pi = gamecontroller.GetComponent<PutIshi>();
        PutPosition pt = position.GetComponent<PutPosition>();
        Matrix mt = matrixdisplay.GetComponent<Matrix>();

        if(pi.turn % 2 == 0){
            aki_x = new int[100];
            aki_z = new int[100];
            aki_1 = 0;
            aki_2 = 0;
            aki_length = 0;

            for(int i = 0; i < mt.goban.GetLength(0); i++){
                for(int j = 0; j < mt.goban.GetLength(0); j++){
                    if(mt.goban[i,j] == 0){
                        aki_x[aki_1] = i;
                        aki_z[aki_2] = j;
                        aki_1 += 1;
                        aki_2 += 1;
                        aki_length += 1;
                    }
                }
            }

            
            if(aki_length > 0){
                // System.Threading.Thread.Sleep(1000);
                Hantei(-1);
            }
        }
    }


    public void Hantei(int l){
        roop = 0;
        PutPosition pt = position.GetComponent<PutPosition>();
        Matrix mt = matrixdisplay.GetComponent<Matrix>();
        PutIshi pi = gamecontroller.GetComponent<PutIshi>();
        CreatePlane cp = planecreate.GetComponent<CreatePlane>();
        Debug.Log("aki_lengthは" + aki_length.ToString());
        while(roop == 0){
            if(aki_length > 0){
                random_x = aki_x[(int)Random.Range(0.0f, (float)(aki_length - 1))];
                random_z = aki_z[(int)Random.Range(0.0f, (float)(aki_length - 1))];
                Debug.Log("random_x:" + random_x.ToString());
                Debug.Log("random_z:" + random_z.ToString());
            }

            if(aki_length == 0){
                Debug.Log("置ける場所がありません 色を変更します。");
                System.Threading.Thread.Sleep(1000);
                pi.turn += 1;
                Debug.Log(pi.turn);
                break;
            }

            if(mt.goban[random_x,random_z] == 0){
                //右方向判定
                for(int i = random_x + 1; i < mt.goban.GetLength(0); i++){
                    // Debug.Log(i.ToString() + "は" + mt.goban[i,random_z].ToString());
                    if(mt.goban[i,random_z] == l){
                        right = i;//右方向の同色石を見つけた場合
                        for(int j = i - 1; j > random_x; j--){
                            if(mt.goban[j,random_z] == -1 * l){
                                right_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                right_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                }

                if(right_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = random_x + 1; i < right; i++){
                        mt.goban[i,random_z] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }
                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    right = 0;
                    right_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }

                //左方向判定
                for(int i = random_x - 1; i > 0; i--){
                    if(mt.goban[i,random_z] == l){
                        left = i;//左方向の同色石を見つけた場合
                        for(int j = i + 1; j < random_x; j++){
                            if(mt.goban[j,random_z] == -1 * l){
                                left_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                left_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                }

                if(left_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = random_x - 1; i > left; i--){
                        mt.goban[i,random_z] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }

                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    left = 0;
                    left_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }

                //上方向判定
                for(int i = random_z + 1; i < mt.goban.GetLength(0); i++){
                    if(mt.goban[random_x,i] == l){
                        up = i;//上方向の同色石を見つけた場合
                        for(int j = i - 1; j > random_z; j--){
                            if(mt.goban[random_x,j] == -1 * l){
                                up_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                up_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                }

                if(up_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = random_z + 1; i < up; i++){
                        mt.goban[random_x,i] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }

                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    up = 0;
                    up_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }
                //下方向判定
                for(int i = random_z - 1; i > 0; i--){
                    if(mt.goban[random_x,i] == l){
                        down = i;//下方向の同色石を見つけた場合
                        for(int j = i + 1; j < random_z; j++){
                            if(mt.goban[random_x,j] == -1 * l){
                                down_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                down_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                }

                if(down_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = random_z - 1; i > down; i--){
                        mt.goban[random_x,i] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }

                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    down = 0;
                    down_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }
                //石再配置部分はまとめられる?

                //右上方向判定
                for(int i = 1; i < mt.goban.GetLength(0) - (int)Mathf.Max((float)random_x, (float)random_z); i++){
                    if(mt.goban[random_x + i,random_z + i] == l){
                        up_right = i;//右上方向の同色石を見つけた場合
                        for(int j = i - 1; j > 0; j--){
                            if(mt.goban[random_x + j,random_z + j] == -1 * l){
                                up_right_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                up_right_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                }

                if(up_right_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = 1; i < up_right; i++){
                        mt.goban[random_x + i,random_z + i] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }
                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    up_right = 0;
                    up_right_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }

                //左下方向判定
                for(int i = 1; i < (int)Mathf.Min((float)random_x, (float)random_z); i++){
                    if(mt.goban[random_x - i,random_z - i] == l){
                        down_left = i;//左下方向の同色石を見つけた場合
                        for(int j = 1; j < down_left; j++){
                            if(mt.goban[random_x - j,random_z - j] == -1 * l){
                                down_left_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                down_left_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                }

                if(down_left_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = 1; i < down_left; i++){
                        mt.goban[random_x - i,random_z - i] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }
                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    down_left = 0;
                    down_left_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }

                //左上方向判定
                while(random_x - up_left_number > 0 && random_z + up_left_number < mt.goban.GetLength(0)){
                    if(mt.goban[random_x - up_left_number, random_z + up_left_number] == l){
                        up_left = up_left_number;//左上方向の同色石を見つけた場合
                        for(int j = 1; j < up_left; j++){
                            if(mt.goban[random_x - j,random_z + j] == -1 * l){
                                up_left_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                up_left_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                    up_left_number += 1;
                }
                up_left_number = 1;

                if(up_left_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = 1; i < up_left; i++){
                        mt.goban[random_x - i,random_z + i] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }
                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    up_left = 0;
                    up_left_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }

                //右下方向判定
                while(random_x + down_right_number < mt.goban.GetLength(0) && random_z - down_right_number > 0){
                    if(mt.goban[random_x + down_right_number,random_z - down_right_number] == l){
                        down_right = down_right_number;//右下方向の同色石を見つけた場合
                        for(int j = 1; j < down_right; j++){
                            if(mt.goban[random_x + j,random_z - j] == -1 * l){
                                down_right_flag = 1;
                                continue;//今の場所の手前まで逆色石があればフラグを立て続ける
                            }
                            else{
                                down_right_flag = 0;
                                break;//1か所でも無かったらフラグは0
                            }
                        }
                        break;
                    }
                    down_right_number += 1;
                }
                down_right_number = 1;

                if(down_right_flag == 1 && Input.GetKeyDown(KeyCode.Space)){
                    for(int i = 1; i < down_right; i++){
                        mt.goban[random_x + i,random_z - i] = l;
                    }
                    pi.Putting_VSComputer();//石置いて行列の値変更
                    
                    GameObject[] Ishis = GameObject.FindGameObjectsWithTag("Ishi");
                    foreach (GameObject Ishi in Ishis) {
                        Destroy(Ishi);//一旦石全部消す
                    }
                    for(int i = 0; i < mt.goban.GetLength(0); i++){
                        //行列の値をもとに石を再配置
                        for(int j = 0; j < mt.goban.GetLength(0); j++){
                            if(mt.goban[i,j] == 1)
                            {
                                Instantiate (cp.white, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                            else if(mt.goban[i,j] == -1){
                                Instantiate (cp.black, new Vector3 (i, 0, j), Quaternion.identity);
                            }
                        }
                    }
                    down_right = 0;
                    down_right_flag = 0;
                    roop = 1;
                    System.Threading.Thread.Sleep(1000);
                }

            }
            aki_x = new int[100];
            aki_z = new int[100];
            aki_1 = 0;
            aki_2 = 0;
            aki_length = 0;
            for(int i = 0; i < mt.goban.GetLength(0); i++){
                for(int j = 0; j < mt.goban.GetLength(0); j++){
                    if(mt.goban[i,j] == 0 && i != random_x && j != random_z){
                        aki_x[aki_1] = i;
                        aki_z[aki_2] = j;
                        aki_1 += 1;
                        aki_2 += 1;
                        aki_length += 1;
                    }
                }
            }

        }
            

        // Debug.Log(random_x);
        // Debug.Log(random_z);
        
    }
}
