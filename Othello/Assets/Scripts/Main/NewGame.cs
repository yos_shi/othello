﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick(){
        // 現在のScene名の取得
        Scene nowscene = SceneManager.GetActiveScene();
        // Sceneの読み直し
        SceneManager.LoadScene(nowscene.name);

    }
}
