﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutPosition : MonoBehaviour
{
    public int putposition_x = 1;
    public int putposition_z = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.W) && putposition_z != 8){
            putposition_z += 1;
            transform.position = new Vector3(putposition_x, 0.1f, putposition_z);
        }

        else if(Input.GetKeyDown(KeyCode.A) && putposition_x != 1){
            putposition_x -= 1;
            transform.position = new Vector3(putposition_x, 0.1f, putposition_z);
        }

        else if(Input.GetKeyDown(KeyCode.S) && putposition_z != 1){
            putposition_z -= 1;
            transform.position = new Vector3(putposition_x, 0.1f, putposition_z);
        }

        else if(Input.GetKeyDown(KeyCode.D) && putposition_x != 8){
            putposition_x += 1;
            transform.position = new Vector3(putposition_x, 0.1f, putposition_z);
        }
        
    }
}
