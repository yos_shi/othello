﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix : MonoBehaviour
{
    public int[,] goban = new int[10,10];
    public string hyouji = "";
    // Start is called before the first frame update
    void Start()
    {
        // //表示確認
        // for(int i = 0; i < goban.GetLength(0); i++){
        //     for(int j = 0; j < goban.GetLength(0); j++){
        //         hyouji += goban[i,j].ToString();
        //     }
        //     Debug.Log(hyouji);
        //     hyouji = "";
        // }

        //壁の作成
        for(int i = 0; i < goban.GetLength(0); i++){
            goban[i,0] = 2;
            goban[0,i] = 2;
            goban[9,i] = 2;
            goban[i,9] = 2;
        }

        //初期配置
        goban[4,4] = 1;
        goban[5,5] = 1;
        goban[4,5] = -1;
        goban[5,4] = -1;

        // kakunin();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return)){
            kakunin();
        }
        
    }

    public void kakunin(){
        for(int i = 0; i < goban.GetLength(0); i++){
            for(int j = 0; j < goban.GetLength(0); j++){
                hyouji += goban[j,i].ToString();
            }
            Debug.Log(i.ToString() + "行目は" + hyouji);
            hyouji = "";
        }

    }
}
