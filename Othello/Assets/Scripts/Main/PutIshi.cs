﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PutIshi : MonoBehaviour
{
    public int turn = 1;
    public GameObject gamecontroller;
    public GameObject matrixdisplay;
    public GameObject position;
    public GameObject planecreate;
    public Text turndisplay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(turn % 2 == 1){
            turndisplay.text = "<color=#FFFFFF>白</color>のターンです。";
        }
        else{
            turndisplay.text = "<color=#000000>黒</color>のターンです。";
        }

        
    }

    public void Putting(){
        Matrix mt = matrixdisplay.GetComponent<Matrix>();
        PutPosition pt = position.GetComponent<PutPosition>();
        CreatePlane cp = planecreate.GetComponent<CreatePlane>();
        if(mt.goban[pt.putposition_x,pt.putposition_z] == 0){
            if(turn % 2 == 1){
                Instantiate (cp.white, new Vector3 (pt.putposition_x, 0, pt.putposition_z), Quaternion.identity);
                mt.goban[pt.putposition_x,pt.putposition_z] = 1;
            }
            else if(turn % 2 == 0){
                Instantiate (cp.black, new Vector3 (pt.putposition_x, 0, pt.putposition_z), Quaternion.identity);
                mt.goban[pt.putposition_x,pt.putposition_z] = -1;    
            }
            turn += 1;
            // mt.kakunin();
        }
    }

    public void Putting_VSComputer(){
        Matrix mt = matrixdisplay.GetComponent<Matrix>();
        PutPosition pt = position.GetComponent<PutPosition>();
        CreatePlane cp = planecreate.GetComponent<CreatePlane>();
        ReturnHantei_VSComputer rvs = gamecontroller.GetComponent<ReturnHantei_VSComputer>();
        if(mt.goban[rvs.random_x,rvs.random_z] == 0){
            if(turn % 2 == 1){
                Instantiate (cp.white, new Vector3 (rvs.random_x, 0, rvs.random_z), Quaternion.identity);
                mt.goban[rvs.random_x,rvs.random_z] = 1;
            }
            else if(turn % 2 == 0){
                Instantiate (cp.black, new Vector3 (rvs.random_x, 0, rvs.random_z), Quaternion.identity);
                mt.goban[rvs.random_x,rvs.random_z] = -1;    
            }
            turn += 1;
            // mt.kakunin();
        }
    }
}
