﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Maisu : MonoBehaviour
{
    public Text shiromaisu;
    public Text kuromaisu;
    public GameObject matrixdisplay;
    public int shiromaisu_kari = 0;
    public int kuromaisu_kari = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Matrix mt = matrixdisplay.GetComponent<Matrix>();
        shiromaisu_kari = 0;
        kuromaisu_kari = 0;
        for(int i = 0; i < mt.goban.GetLength(0); i++){
            for(int j = 0; j < mt.goban.GetLength(0); j++){
                if(mt.goban[i,j] == 1){
                    shiromaisu_kari += 1;
                }
                else if(mt.goban[i,j] == -1){
                    kuromaisu_kari += 1;
                }

            }
        }
        // Debug.Log("白の枚数:" + shiromaisu_kari);
        // Debug.Log("黒の枚数" + kuromaisu_kari);
        shiromaisu.text = "白の枚数:" + shiromaisu_kari.ToString();
        kuromaisu.text = "黒の枚数:" + kuromaisu_kari.ToString();
        
    }
}
