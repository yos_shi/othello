﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePlane : MonoBehaviour
{
    public GameObject masu1;
    public GameObject masu2;
    public GameObject white;
    public GameObject black;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 1; i <= 8; i++){
            for(int j = 1; j <= 8; j++){
                if((i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1)){
                    Instantiate (masu1, new Vector3 (i, 0, j), Quaternion.identity);
                }
                else{
                    Instantiate (masu2, new Vector3 (i, 0, j), Quaternion.identity);
                }
            }
        }

        Instantiate (white, new Vector3 (4, 0, 4), Quaternion.identity);
        Instantiate (white, new Vector3 (5, 0, 5), Quaternion.identity);
        Instantiate (black, new Vector3 (4, 0, 5), Quaternion.identity);
        Instantiate (black, new Vector3 (5, 0, 4), Quaternion.identity);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
